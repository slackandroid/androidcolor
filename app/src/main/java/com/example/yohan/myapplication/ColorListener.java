package com.example.yohan.myapplication;

import android.view.View;

/**
 * Created by Yohan on 16/03/2017.
 */

public interface ColorListener{
    public void OnColorClick(View v, int color);
}
